﻿namespace PipelineExample
{
    /// <summary>
    /// This is a representation of the operations
    /// </summary>
    /// <typeparam name="T">Data type of the operation</typeparam>
    public interface IFilter<T>
    {
        /// <summary>
        /// Execute the operation
        /// </summary>
        /// <param name="input">The beginning object to make operations on</param>
        /// <returns>The type of the operation</returns>
        T Execute(T input);
    }
}