﻿namespace PipelineExample
{
    public class DoorsFilter : IFilter<ICar>
    {
        public ICar Execute(ICar input)
        {
            input.NbDoors = 5;
            return input;
        }
    }
}