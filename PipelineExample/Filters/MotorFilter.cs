﻿namespace PipelineExample
{
    public class MotorFilter : IFilter<ICar>
    {
        public ICar Execute(ICar input)
        {
            input.MotorSetup = true;
            return input;
        }
    }
}