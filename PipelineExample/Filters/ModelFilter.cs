﻿namespace PipelineExample
{
    public class ModelFilter : IFilter<ICar>
    {
        public ICar Execute(ICar input)
        {
            if (input.Brand == "Chevrolet")
            {
                input.Model = "gvx350";
            }
            else if (input.Brand == "Ford")
            {
                input.Model = "fcv550";
            }
            else if (input.Brand == "Mitsubishi")
            {
                input.Model = "mit753";
            }
            return input;
        }
    }
}