﻿namespace PipelineExample
{
    public class WheelFilter : IFilter<ICar>
    {
        public ICar Execute(ICar input)
        {
            input.NbWheels = 4;
            return input;
        }
    }
}