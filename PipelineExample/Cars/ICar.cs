﻿namespace PipelineExample
{
    public interface ICar
    {
        string Model { get; set; }
        bool MotorSetup { get; set; }
        int NbDoors { get; set; }
        int NbWheels { get; set; }
        int Id { get; set; }
        string Brand { get; set; }
        string Type { get; set; }
    }
}