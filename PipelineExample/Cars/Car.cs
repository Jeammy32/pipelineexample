﻿using System.Globalization;

namespace PipelineExample
{
    public class Car : ICar
    {
        public string Model { get; set; }
        public bool MotorSetup { get; set; }
        public int NbDoors { get; set; }
        public int NbWheels { get; set; }
        
        public int Id { get; set; }
        public string Brand { get; set; }
        public string Type { get; set; }
        
        public Car(int id, string brand, string type)
        {
            this.Id = id;
            this.Brand = brand;
            this.Type = type;
            this.Model = "N/A";
        }
    }
}