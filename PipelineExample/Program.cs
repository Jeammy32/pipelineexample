﻿using System;
using PipelineExample.Pipeline;

namespace PipelineExample
{
    /// <summary>
    /// This Pipeline pattern example is inspired from the example provided by Akkiraju Ivaturi on https://www.c-sharpcorner.com/UploadFile/akkiraju/pipes-and-filters-pattern/
    /// </summary>
    internal class Program
    {
        public static void Main(string[] args)
        {
            ICar car1 = new Car(00001, "Chevrolet", "Sport");
            ICar car2 = new Car(00002, "Ford", "Berline");
            ICar car3 = new Car(00003, "Mitsubishi", "SUV");

            SendPipeline sendPipelineType1 = new SendPipeline(true,true,true,true);
            SendPipeline sendPipelineType2 = new SendPipeline(true,false,false,true);
            
            var publishedCar1 = sendPipelineType1.PerformFilter(car1);
            var publishedCar2 = sendPipelineType2.PerformFilter(car2);
            var publishedCar3 = sendPipelineType1.PerformFilter(car3);
            
            Console.WriteLine("The car {1} {2} have {0} doors.", publishedCar1.NbDoors, publishedCar1.Brand, publishedCar1.Type);
            Console.WriteLine("Have a model : {0}", publishedCar1.Model);
            Console.WriteLine("Have a motor setup : {0}", publishedCar1.MotorSetup);
            Console.WriteLine("Have {0} wheels", publishedCar1.NbWheels);
            Console.WriteLine("============================================================");
            Console.WriteLine("The car {1} {2} have {0} doors.", publishedCar2.NbDoors, publishedCar2.Brand, publishedCar2.Type);
            Console.WriteLine("Have a model : {0}", publishedCar2.Model);
            Console.WriteLine("Have a motor setup: {0}", publishedCar2.MotorSetup);
            Console.WriteLine("Have {0} wheels", publishedCar2.NbWheels);
            Console.WriteLine("============================================================");
            Console.WriteLine("The car {1} {2} have {0} doors.", publishedCar3.NbDoors, publishedCar3.Brand, publishedCar3.Type);
            Console.WriteLine("Have a model : {0}", publishedCar3.Model);
            Console.WriteLine("Have a motor setup: {0}", publishedCar3.MotorSetup);
            Console.WriteLine("Have {0} wheels", publishedCar3.NbWheels);
        }
    }
}