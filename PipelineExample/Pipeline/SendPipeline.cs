﻿using PipelineExample.Pipeline;

namespace PipelineExample
{
    public class SendPipeline : Pipeline<ICar>
    {
        private bool isCarDoorsSet = false;
        private bool isCarModelSet = false;
        private bool isCarMotorSet = false;
        private bool isCarWheelsSet = false;
        public bool IsCarDoorsSet => isCarDoorsSet;
        public bool IsCarModelSet => isCarModelSet;
        public bool IsCarMotorSet => isCarMotorSet;
        public bool IsCarWheelsSet => isCarWheelsSet;
        
        public SendPipeline(bool setDoors, bool setModel, bool setMotorSetup, bool setWheels)
        {
            if (setDoors)
            {
                Register(new DoorsFilter());
                isCarDoorsSet = true;
            }

            if (setModel)
            {
                Register(new ModelFilter());
                isCarModelSet = true;
            }

            if (setMotorSetup)
            {
                Register(new MotorFilter());
                isCarMotorSet = true;
            }
            
            if (setWheels)
            {
                Register(new WheelFilter());
                isCarWheelsSet = true;
            }
        }
    }
}