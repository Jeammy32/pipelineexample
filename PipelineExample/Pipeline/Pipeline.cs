﻿using System.Collections.Generic;
using System.Linq;

namespace PipelineExample.Pipeline
{
    public abstract class Pipeline<T>
    {
        private readonly List<IFilter<T>> filters = new List<IFilter<T>>();

        public Pipeline<T> Register(IFilter<T> filter)
        {
            this.filters.Add(filter);
            return this;
        }

        public T PerformFilter(T car)
        {
            return this.filters.Aggregate(car, (current, filter) => filter.Execute(current));
        }
    }
}